import { createTheme } from '@mui/material/styles';

import { CMY } from './typings/constants';

declare module '@mui/material/styles' {
  interface ThemeOptions {
    colors?: CMY,
  }
  interface Theme {
    colors?: CMY,
  }
}

export const DEFAULT_THEME_COLORS = { // fancy hip colors of the spinner
  c: '#09b3e3', // cyan
  m: '#da3ab3', // magenta
  y: '#fec843', // yellow
};

export const DEFAULT_THEME = createTheme({
  colors: DEFAULT_THEME_COLORS,
  palette: {
    action: {
      activatedOpacity: .36,
      selectedOpacity: .24,
    },
    warning: { main: '#fda728' }, // lightened orange
    primary: { main: '#6d1d59' }, // darkened purple
    secondary: { main: '#2c8fb8' }, // darkened blue
  },
});

export const ITEM_ACTIONS = {
  CREATE: 'create',
  READ: 'read',
  UPDATE: 'update',
  DELETE: 'delete',
};

const constants = {
  DEFAULT_THEME_COLORS,
  DEFAULT_THEME,
  ITEM_ACTIONS,
};
export default constants;
