export interface CMY {
  c: string,
  m: string,
  y: string,
}

export interface ITEM_ACTIONS {
  CREATE: 'create',
  READ: 'read',
  UPDATE: 'update',
  DELETE: 'delete',
}
