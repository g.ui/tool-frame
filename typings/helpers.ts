import { ChangeEvent, KeyboardEvent, MouseEvent } from 'react';

export type Async<T> = T | Promise<T>;

export type InputChangeEvent = ChangeEvent<HTMLInputElement | HTMLTextAreaElement>;

export type KeyEvent = KeyboardEvent<HTMLElement>;

export type ClickEvent = MouseEvent<HTMLElement>;

export type AmendedThemeColor = 'primary' | 'secondary' | 'error' | 'warning' | 'info' | 'success';
