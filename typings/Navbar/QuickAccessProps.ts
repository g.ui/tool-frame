import { NavbarProps } from './NavbarProps';

export interface QuickAccessProps {
  items?: NavbarProps['collections'],
  iconsOnly?: boolean,
}

export default QuickAccessProps;
