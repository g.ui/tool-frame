import { AppBarProps, SxProps } from '@mui/material';
import { ElementType } from 'react';

import { NavbarBreadcrumbsProps } from './NavbarBreadcrumbsProps';
import { ToolFrameProps } from '../ToolFrameProps';

export interface NavbarProps extends Omit<AppBarProps, 'color'> {
  collections?: ToolFrameProps['collections'],
  application?: ToolFrameProps['application'],
  breadcrumbs?: true | NavbarBreadcrumbsProps['breadcrumbs'],
  quickAccess?: boolean | 'iconsOnly' | ElementType,
  loading?: ToolFrameProps['loading'],
  position?: 'fixed' | 'sticky' | 'absolute' | 'static' | 'relative',
  color?: 'inherit' | 'primary' | 'secondary' | 'default' | 'transparent' | 'clear',
  sx?: SxProps,
  onMenuOpen?: () => unknown,
  onLogout?: ToolFrameProps['onLogout'],
}

export default NavbarProps;
