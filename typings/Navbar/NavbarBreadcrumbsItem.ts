import { Breakpoint, ListItemButtonProps } from '@mui/material';

import { Item } from '..';

export interface NavbarBreadcrumbsItemProps extends ListItemButtonProps {
  name?: Item['name'],
  icon?: Item['icon'],
  link?: Item['link'],
  truncation?: number,
  iconDisplayThreshold?: Breakpoint,
}

export default NavbarBreadcrumbsItemProps;
