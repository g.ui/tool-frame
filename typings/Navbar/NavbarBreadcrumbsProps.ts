import { Breakpoint } from '@mui/material';
import { ReactElement } from 'react';

import { NavbarProps } from './NavbarProps';
import { NavbarBreadcrumbsItemProps } from './NavbarBreadcrumbsItem';

export interface NavbarBreadcrumbsProps {
  application?: NavbarProps['application'],
  collections?: NavbarProps['collections'],
  breadcrumbs?: false | NavbarBreadcrumbsItemProps[] | ReactElement,
  fallbackToLocation?: boolean,
  truncation?: NavbarBreadcrumbsItemProps['truncation'],
  displayThreshold?: Breakpoint,
  onMenuOpen?: () => unknown,
}

export default NavbarBreadcrumbsProps;
