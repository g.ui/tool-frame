import { Breakpoint } from '@mui/material';
import { BoxProps } from '@mui/system';

type Direction = 'center' | 'left' | 'right' | 'top' | 'bottom';

export interface ContentProps extends Omit<BoxProps, 'position' | 'maxWidth'> {
  initializing?: boolean,
  maxWidth?: false | Breakpoint,
  elevation?: number,
  position?: Direction | `${Direction} ${Direction}`,
  align?: 'left' | 'center' | 'right',
}

export default ContentProps;
