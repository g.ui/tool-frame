import { ToolFrameProps } from '../ToolFrameProps';

export interface ContentNotFoundProps {
  title?: string,
  logo?: ToolFrameProps['logo'],
  backButtonText?: string,
}

export default ContentNotFoundProps;
