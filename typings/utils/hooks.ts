import { ProviderContext } from 'notistack';

export type AutoControllCondition = (current?: any) => boolean;
export type AutoControllCallback = (current?: any) => unknown;
export type AutoControllCallbacks =
  AutoControllCallback[] | [AutoControllCallback, AutoControllCondition][];

export type useNotifiyType = ProviderContext['enqueueSnackbar'];
