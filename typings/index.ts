import { ElementType } from 'react';

import { Async } from './helpers';

export { ITEM_ACTIONS } from '../constants';

export type ItemId = string | number;
export interface Item {
  id: ItemId,
  name?: string,
  icon?: ElementType,
  link?: string,
}

export type CollectionId = string | number;
export type CollectionEntity = string;
export interface Collection {
  id?: CollectionId,
  entity: CollectionEntity,
  iconSingular?: ElementType,
  icon?: ElementType,
  link?: string,
  items?: Item[],
  expanded?: boolean,
  onEditing?: (collectionIdOrUnmark: CollectionId | false) => Async<any>,
  onCollapse?: (collectionId: CollectionId) => Async<any>,
  onExpand?: (collectionId: CollectionId) => Async<any>,
}

export interface CollectionItem {
  id?: string | number,
}
