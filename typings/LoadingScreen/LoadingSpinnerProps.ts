import { PropsWithRef } from 'react';

export interface LoadingSpinnerProps extends PropsWithRef<{}> {
  size?: string | number,
  rotate?: boolean,
}

export default LoadingSpinnerProps;
