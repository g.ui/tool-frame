import { BackdropProps } from '@mui/material';
import { ElementType } from 'react';

export interface LoadingScreenProps extends Omit<BackdropProps, 'open'> {
  opened?: boolean,
  spinner?: false | ElementType,
  grayscale?: number,
  brightness?: number,
  blur?: number,
  // override due to prop renaming (for external use)
  open?: BackdropProps['open'],
}

export default LoadingScreenProps;
