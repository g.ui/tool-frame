import { Theme } from '@mui/material';
import { ElementType, PropsWithChildren } from 'react';

import { Collection, CollectionId, CollectionItem } from '.';
import { Async } from './helpers';
import { LoginProps } from './Login/LoginProps';
import { NavbarProps } from './Navbar/NavbarProps';
import { MenuProps } from './Menu/MenuProps';
import { ITEM_ACTIONS } from './constants';

type OnItemChangeFunction = (
  collectionId: CollectionId,
  action: ITEM_ACTIONS,
  item?: CollectionItem,
) => Async<any>

export interface ToolFrameProps extends PropsWithChildren<{}> {
  initializing?: boolean,
  loading?: boolean,
  auth?: boolean,
  application?: string,
  theme?: Theme,
  collections?: Collection[],
  logo?: ElementType,
  menu?: boolean | ElementType,
  navbar?: boolean | ElementType,
  quickAccess: NavbarProps['quickAccess'],
  login?: boolean | ElementType,
  loadingScreen?: boolean | ElementType,
  contentNotFound?: boolean | ElementType,
  menuProps?: MenuProps,
  navbarProps?: NavbarProps,
  onItemChange?: OnItemChangeFunction,
  onLogin: LoginProps['onSubmit'],
  onLogout?: () => Async<any>,
}

export default ToolFrameProps;
