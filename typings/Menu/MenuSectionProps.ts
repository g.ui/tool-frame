import { ElementType } from 'react';

import { Collection, CollectionEntity, Item } from '..';
import { MenuProps } from './MenuProps';

export interface MenuSectionProps {
  entity: CollectionEntity,
  items?: Item[],
  icon?: ElementType,
  editable?: boolean,
  amendable?: boolean,
  editing?: boolean,
  expandable?: boolean,
  expanded?: boolean,
  onItemChange?: MenuProps['onItemChange'],
  onEditing?: Collection['onEditing'],
  onCollapse?: Collection['onCollapse'],
  onExpand?: Collection['onExpand'],
}

export default MenuSectionProps;
