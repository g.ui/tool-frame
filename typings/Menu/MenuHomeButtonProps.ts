import { ListItemButtonProps } from '@mui/material';

import { ToolFrameProps } from '../ToolFrameProps';

export interface MenuHomeButtonProps extends ListItemButtonProps {
  logo?: ToolFrameProps['logo'],
  disabled?: boolean,
}

export default MenuHomeButtonProps;
