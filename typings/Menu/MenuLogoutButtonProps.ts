import { ListItemButtonProps } from '@mui/material';

import { ToolFrameProps } from '../ToolFrameProps';

export interface MenuLogoutButtonProps extends ListItemButtonProps {
  text?: string,
  onClick?: ToolFrameProps['onLogout'],
}

export default MenuLogoutButtonProps;
