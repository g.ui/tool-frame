import { SwipeableDrawerProps } from '@mui/material';
import { ElementType, ReactNode } from 'react';

import { MenuHomeButtonProps } from './MenuHomeButtonProps';
import { ToolFrameProps } from '../ToolFrameProps';

export interface MenuProps extends Omit<SwipeableDrawerProps, 'open' | 'onOpen' | 'onClose'> {
  logo?: MenuHomeButtonProps['logo'],
  opened?: boolean,
  width?: number,
  homeButton?: false | ElementType,
  collections?: ToolFrameProps['collections'],
  mutexEditing?: boolean,
  mutexExpand?: boolean,
  topChildren?: ReactNode | ReactNode[],
  bottomChildren?: ReactNode | ReactNode[],
  logoutButton?: false | ElementType,
  logoutButtonPosition?: number,
  onItemChange?: ToolFrameProps['onItemChange'],
  onOpen?: () => unknown,
  onClose?: () => unknown,
  onLogout?: ToolFrameProps['onLogout'],
  // override due to prop renaming (for external use)
  open?: SwipeableDrawerProps['open'],
}

export default MenuProps;
