import { ListItemButtonProps } from '@mui/material';
import { ClickEvent } from '../helpers';
import { CollectionEntity, Item } from '..';

export interface MenuSectionItemProps extends ListItemButtonProps {
  entity: CollectionEntity,
  item?: Item,
  selected?: boolean,
  deletable?: boolean,
  onClick?: (event?: ClickEvent) => unknown,
  onDelete?: () => unknown,
}

export default MenuSectionItemProps;
