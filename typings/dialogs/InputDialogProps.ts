import { DialogProps } from '@mui/material';

import { AmendedThemeColor, Async, KeyEvent } from '../helpers';

export interface InputDialogProps extends Omit<DialogProps, 'open'> {
  opened?: boolean,
  entity?: string,
  title?: string,
  text?: string,
  cancelButtonText?: string,
  confirmButtonText?: string,
  color?: AmendedThemeColor,
  confirmOnEnter?: boolean,
  validation?: (current?: string) => Async<boolean>,
  onClose?: () => unknown,
  onConfirm?: (value: string) => Async<unknown>,
  onKeyUp?: (event?: KeyEvent) => unknown,
  // override due to prop renaming (for external use)
  open?: DialogProps['open'],
}

export default InputDialogProps;
