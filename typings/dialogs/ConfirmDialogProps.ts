import { DialogProps } from '@mui/material';

import { AmendedThemeColor, KeyEvent } from '../helpers';

export interface ConfirmDialogProps extends Omit<DialogProps, 'open'> {
  opened?: boolean,
  title?: string,
  text?: string,
  cancelButtonText?: string,
  confirmButtonText?: string,
  color?: AmendedThemeColor,
  confirmOnEnter?: boolean,
  onClose?: () => unknown,
  onConfirm?: () => unknown,
  onKeyUp?: (event: KeyEvent) => unknown,
  // override due to prop renaming (for external use)
  open?: DialogProps['open'],
}

export default ConfirmDialogProps;
