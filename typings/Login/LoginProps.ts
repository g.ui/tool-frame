import { ElementType } from 'react';

import { AmendedThemeColor, Async } from '../helpers';
import { ToolFrameProps } from '../ToolFrameProps';

export interface LoginCredentials {
  username: string,
  password: string,
}

export interface LoginProps {
  checking?: boolean,
  logo?: ToolFrameProps['logo'],
  color?: AmendedThemeColor,
  loadingSpinner?: ElementType,
  application?: ToolFrameProps['application'],
  headerText?: string,
  usernameLabel?: string,
  usernameAutocomplete?: string,
  passwordLabel?: string,
  loginButtonText?: string,
  onSubmit?: (credentials: LoginCredentials) => Async<any>,
}

export default LoginProps;
