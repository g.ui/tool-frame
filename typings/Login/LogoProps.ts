import { AvatarProps } from '@mui/material';
import { ElementType } from 'react';

import { AmendedThemeColor } from '../helpers';

export interface LogoProps extends AvatarProps {
  icon?: ElementType,
  size?: number,
  color?: AmendedThemeColor | string,
}

export default LogoProps;
