import ConfirmDialog from './ConfirmDialog';
import InputDialog from './InputDialog';
const dialogs = {
  ConfirmDialog,
  InputDialog,
};
export default dialogs;

export { default as ConfirmDialog } from './ConfirmDialog';
export { default as InputDialog } from './InputDialog';

export * from './ConfirmDialog';
export * from './InputDialog';
