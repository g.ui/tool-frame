import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  Zoom,
  TextField,
} from '@mui/material';
import truncate from 'cli-truncate';
import { useEffect, useRef, useState, FunctionComponent } from 'react';
import { titleCase } from 'title-case';

import { useAutoControlled, useColor, useNotify } from '../utils/hooks';
import { asyncify } from '../utils/misc';
import { ClickEvent, InputChangeEvent, KeyEvent } from '../typings/helpers';
import { InputDialogProps } from '../typings/dialogs/InputDialogProps';

const noop = () => {};

export const InputDialog: FunctionComponent<InputDialogProps> = ({
  opened = false,
  entity = '',
  title = '',
  text = 'Please provide below information to proceed!',
  cancelButtonText = 'Cancel',
  confirmButtonText = 'OK',
  color = 'secondary',
  confirmOnEnter = true,
  validation = val => !!val?.trim(),
  onClose = noop,
  onConfirm = noop,
  onKeyUp = noop,
  ...props
}) => {
  const notifyError = useNotify('error');

  // state management
  const $color = useColor(color);
  const [$opened, set$opened] = useAutoControlled(opened, [
    [onClose, opened => !opened],
  ]);
  const [value, setValue] = useState('');
  const [error, setError] = useState(false);
  const input = useRef<HTMLElement>(null);

  function focus() {
    input.current?.focus();
  }
  useEffect(() => $opened && focus(), [$opened]);

  // clean props
  const _entity = truncate(titleCase(entity), 30, { position: 'middle' });
  const _title = title || `${_entity} information needed`;
  const _validation = asyncify(validation);
  const confirmation = asyncify(onConfirm);

  // event handlers
  function onChange(event: InputChangeEvent) {
    setError(false);
    setValue(event.target.value);
  }

  function noClick(event: ClickEvent) {
    event.stopPropagation();
  }

  function _onClose(event: ClickEvent) {
    noClick(event);
    set$opened(false);
  }

  async function _validate(val: string) {
    const res = await _validation(val)
      .catch(err => err);
    if (res === true) return true;
    notifyError(`Input validation failed: ${res.message || res || 'please revise and retry!'}`);
    return res;
  }

  async function _onConfirm(event?: ClickEvent) {
    if (event) noClick(event);
    const res = await _validate(value.trim());
    if (res !== true) return setError(true);
    await confirmation(value);
    set$opened(false);
  }

  function _onKeyUp(event: KeyEvent) {
    onKeyUp(event);
    if (confirmOnEnter && event.key === 'Enter') _onConfirm();
  }

  return (
    <Dialog
      TransitionComponent={Zoom}
      {...props}
      open={$opened}
      keepMounted
      onClose={_onClose}
      onBackdropClick={_onClose}
      onKeyUp={_onKeyUp}>
      <DialogTitle onClick={focus} sx={{ background : $color?.main, color: $color?.contrastText }}>
        {_title}
      </DialogTitle>
      <Divider />
      <DialogContent onClick={focus}>
        <DialogContentText>{text}</DialogContentText>
        <TextField
          margin="dense"
          onChange={onChange}
          error={error}
          label={_entity}
          type="text"
          inputRef={input}
          fullWidth
          variant="standard"
          color={color}
        />
      </DialogContent>
      <DialogActions onClick={focus}>
        <Button color={color} onClick={_onClose}>{cancelButtonText}</Button>
        <Button
          color={color}
          onClick={_onConfirm}
          variant="contained"
          disabled={!value}>
          {confirmButtonText}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default InputDialog;
