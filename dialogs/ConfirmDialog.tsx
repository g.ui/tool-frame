import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  Zoom,
  useTheme,
} from '@mui/material';
import { FunctionComponent } from 'react';

import { useAutoControlled, useColor } from '../utils/hooks';
import { ClickEvent, KeyEvent } from '../typings/helpers';
import { ConfirmDialogProps } from '../typings/dialogs/ConfirmDialogProps';

declare module '@mui/material/styles' {
  interface Palette {
    type: 'light' | 'dark',
  }
}

const noop = () => {};

export const ConfirmDialog: FunctionComponent<ConfirmDialogProps> = ({
  opened = false,
  title = 'Are you sure?',
  text = 'Do you really want to perform this action?',
  cancelButtonText = 'Cancel',
  confirmButtonText = 'OK',
  color = 'warning',
  confirmOnEnter = false,
  onClose = noop,
  onConfirm = noop,
  onKeyUp = noop,
  ...props
}) => {
  const theme = useTheme();
  const $color = useColor(color);

  // state management
  const [$opened, set$opened] = useAutoControlled(opened, [
    [onClose, opened => !opened],
  ]);

  // clean props
  const background = theme.palette.type === 'dark' ? $color?.dark : $color?.light;

  // event handlers
  function noClick(event: ClickEvent) {
    event.stopPropagation();
  }

  function _onClose(event: ClickEvent) {
    noClick(event);
    set$opened(false);
  }

  function _onConfirm(event?: ClickEvent) {
    if (event) noClick(event);
    onConfirm();
    set$opened(false);
  }

  function _onKeyUp(event: KeyEvent) {
    onKeyUp(event);
    if (confirmOnEnter && event.key === 'Enter') _onConfirm();
  }

  return (
    <Dialog
      TransitionComponent={Zoom}
      {...props}
      keepMounted
      onClose={_onClose}
      onKeyUp={_onKeyUp}
      open={$opened}>
      <DialogTitle onClick={noClick} style={{ background, color: $color?.contrastText }}>
        {title}
      </DialogTitle>
      <Divider />
      <DialogContent onClick={noClick}>
        <DialogContentText>{text}</DialogContentText>
      </DialogContent>
      <DialogActions onClick={noClick}>
        <Button color={color} onClick={_onClose}>{cancelButtonText}</Button>
        <Button color={color} onClick={_onConfirm} variant="contained">{confirmButtonText}</Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmDialog;
