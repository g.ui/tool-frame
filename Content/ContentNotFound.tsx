import { Button, Typography } from '@mui/material';
import { Fragment, FunctionComponent } from 'react';
import { useNavigate } from 'react-router-dom';

import { Content } from './Content';
import { Logo as DefaultLogo } from '../Login';
import { ContentNotFoundProps } from '../typings/Content/ContentNotFoundProps';

export const ContentNotFound: FunctionComponent<ContentNotFoundProps> = ({
  title = 'Page not found!',
  logo = DefaultLogo,
  backButtonText = 'Back to home',
  ...props
}) => {
  const navigate = useNavigate();

  // clean props
  const Logo = logo || Fragment;

  return (
    <Content maxWidth="xs" align="center" {...props}>
      <Typography variant="h5" sx={{ mb: 2 }}>{title}</Typography>
      {logo && <Logo size={200} />}
      {backButtonText && <Button onClick={() => navigate('/')} sx={{ mt: 2 }}>
        {backButtonText}
      </Button>}
    </Content>
  );
};

export default ContentNotFound;
