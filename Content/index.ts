import Content from './Content';
export default Content;

export { default as Content } from './Content';
export { default as ContentNotFound } from './ContentNotFound';

export * from './Content';
export * from './ContentNotFound';
