import { useTheme, Container, Paper } from '@mui/material';
import { Box } from '@mui/system';
import CSS from 'csstype'
import { FunctionComponent } from 'react';

import { LoadingScreen } from '../LoadingScreen';
import { useBreakpoint } from '../utils/hooks';
import { ContentProps } from '../typings/Content/ContentProps';

export const Content: FunctionComponent<ContentProps> = ({
  initializing = false,
  maxWidth = false,
  elevation = maxWidth === false ? 0 : 4,
  position = 'center',
  align = undefined,
  sx = {},
  children = [],
  ...props
}) => {
  const theme = useTheme();
  const sm = useBreakpoint('sm');

  // clean props
  const fullsize = maxWidth === false;
  const pos = { x: 'center', y: 'center' };
  if (position.includes('left')) pos.x = 'start';
  if (position.includes('right')) pos.x = 'end';
  if (position.includes('top')) pos.y = 'start';
  if (position.includes('bottom')) pos.y = 'end';

  // styling
  const fullHeight = `calc(100vh - ${sm ? 64 : 56}px)`;
  const wrapperSx = { width: '100vw', minHeight: fullHeight, display: 'flex' };
  const containerSx = { p: '0 !important' };
  const paperStyles = { boxSizing: 'border-box' as CSS.Property.BoxSizing, width: '100%' };
  const boxSx = { display: 'flex' };

  // reactive styling
  const alignItems = { left: 'start', center: 'center', right: 'end', 0: undefined }[align || 0];
  const flexDirection = align ? 'column' : 'row';
  const minHeight = fullsize ? fullHeight : undefined;

  return (
    <Box sx={{ ...wrapperSx, justifyContent: pos.x, alignItems: fullsize ? 'start' : pos.y }}>
      <LoadingScreen
        opened={initializing}
        spinner={false}
        sx={{ zIndex: theme.zIndex.appBar - 1 }} />
      <Container
        maxWidth={maxWidth}
        fixed={!fullsize}
        sx={{ ...containerSx, minHeight, m: fullsize ? 0 : 3 }}>
        <Paper
          elevation={elevation}
          style={{ ...paperStyles, minHeight, padding: theme.spacing(fullsize ? 0 : 2) }}>
          <Box {...props} sx={{ ...boxSx, minHeight, alignItems, flexDirection, ...sx }}>
            {children}
          </Box>
        </Paper>
      </Container>
    </Box>
  );
};

export default Content;
