import { Close } from '@mui/icons-material';
import { ThemeProvider, Collapse, IconButton } from '@mui/material';
import { SnackbarKey, SnackbarProvider } from 'notistack';
import { createRef, FunctionComponent, useState, Fragment } from 'react';
import { BrowserRouter, Navigate, Routes, Route, useLocation } from 'react-router-dom';

import { DEFAULT_THEME } from './constants';
import { ContentNotFound as DefaultContentNotFound } from './Content';
import { LoadingScreen as DefaultLoadingScreen } from './LoadingScreen';
import { Login as DefaultLogin, Logo as DefaultLogo } from './Login';
import { Menu as DefaultMenu } from './Menu';
import { Navbar as DefaultNavbar } from './Navbar';
import { useSnackbarStyles, useAutoControlled } from './utils/hooks';
import { asyncify } from './utils/misc';
import { LoginCredentials } from './typings/Login/LoginProps';
import { ToolFrameProps } from './typings/ToolFrameProps';

const noop = () => {};

/* eslint complexity: [2, 23] */
export const ToolFrame: FunctionComponent<ToolFrameProps> = ({
  initializing = false,
  loading = false,
  auth = false,
  application = '',
  theme = DEFAULT_THEME,
  collections = [],
  logo = DefaultLogo,
  menu = DefaultMenu,
  navbar = DefaultNavbar,
  quickAccess,
  login = DefaultLogin,
  loadingScreen = DefaultLoadingScreen,
  contentNotFound = DefaultContentNotFound,
  menuProps = {},
  navbarProps = {},
  onItemChange = noop,
  onLogin = noop,
  onLogout = noop,
  children = [],
}) => {
  const snackbarClasses = useSnackbarStyles({ theme });

  // state management
  const [$auth, set$auth] = useAutoControlled(auth);
  const [menuOpened, setMenuOpened] = useState(false);
  const snackbar = createRef<SnackbarProvider>();

  // clean props
  const Menu = menu === true ? DefaultMenu : (menu || Fragment);
  const Navbar = navbar === true ? DefaultNavbar : (navbar || Fragment);
  const Login = login === true ? DefaultLogin : (login || Fragment);
  const LoadingScreen = loadingScreen === true ? DefaultLoadingScreen : (loadingScreen || Fragment);
  const ContentNotFound = contentNotFound === true
    ? DefaultContentNotFound
    : (contentNotFound || Fragment);

  const _onLogin = asyncify(onLogin);
  const _onLogout = asyncify(onLogout);

  // event handlers
  function $onLogin(credentials: LoginCredentials) {
    return _onLogin(credentials)
      .then(res => { // set auth to true if login function does not throw
        set$auth(true);
        return res;
      });
  }
  function $onLogout() {
    return _onLogout()
      .then(res => { // set auth to false if logout function does not throw
        set$auth(false);
        return res;
      });
  }

  // sub props with defaults
  const _menuProps = {
    logo,
    collections,
    onItemChange,
    onLogout: $onLogout,
    ...menuProps,
  };
  const _navbarProps = {
    application,
    collections,
    quickAccess,
    loading,
    onLogout: $onLogout,
    ...navbarProps,
  };

  // snackbar props
  function DismissButton(key: SnackbarKey) {
    function onClick() {
      return snackbar.current?.closeSnackbar(key);
    }
    return (
      <IconButton onClick={onClick}>
        <Close />
      </IconButton>
    );
  }

  // routes when not authorized
  // const loginElement = <Login application={application} logo={logo} onSubmit={$onLogin} />;
  const LoginRedirect = () => (<Navigate to={`/?r=${useLocation().pathname}`} />);
  const nonAuthRoutes = [
    <Route key="login" index element={<Login
      application={application}
      logo={logo}
      onSubmit={$onLogin} />} />,
    <Route key="redirect" path="/*" element={<LoginRedirect />} />,
  ];

  // 404 route
  const notFoundContent = <ContentNotFound logo={logo} />;
  const notFoundRoute = <Route key="not-found" path="*" element={notFoundContent} />;

  return (
    <ThemeProvider theme={theme}>
      <SnackbarProvider
        ref={snackbar}
        action={DismissButton}
        TransitionComponent={Collapse}
        classes={{ root: snackbarClasses.root }}
        preventDuplicate>
        <BrowserRouter>
          {loadingScreen && <LoadingScreen opened={initializing} />}
          {($auth || !login) && <>
            {menu && <Menu
              opened={menuOpened}
              onOpen={() => setMenuOpened(true)}
              onClose={() => setMenuOpened(false)}
              {..._menuProps} />}
            {navbar && <Navbar
              onMenuOpen={() => setMenuOpened(true)}
              {..._navbarProps} />}
          </>}
          <Routes>
            {$auth || !login ? children : null}
            {contentNotFound && ($auth || !login) && notFoundRoute}
            {!$auth && login && nonAuthRoutes}
          </Routes>
        </BrowserRouter>
      </SnackbarProvider>
    </ThemeProvider>
  );
};

export default ToolFrame;
