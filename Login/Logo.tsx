import { AppsOutage } from '@mui/icons-material';
import { Avatar, useTheme } from '@mui/material';
import { FunctionComponent } from 'react';

import { useColor } from '../utils/hooks';
import { DEFAULT_THEME_COLORS } from '../constants';
import { LogoProps } from '../typings/Login/LogoProps';

const baseSize = 40;

export const Logo: FunctionComponent<LogoProps> = ({
  icon = AppsOutage,
  size = baseSize,
  color = '#f8f8f8ee',
  sx = {},
  ...props
}) => {
  const { colors } = useTheme();
  const { c, m, y } = colors || DEFAULT_THEME_COLORS;

  // clean props
  const Icon = icon;
  const x = size / baseSize;
  const border = `${x * .4 < .5 ? .5 : x * .4}px solid ${m}`;
  const boxShadow = `0px 0px ${x * 1.6}px ${x * .8}px ${c}88`;
  const _color = useColor(color);

  const background = `linear-gradient(217deg, ${c} 10%, ${c}00 60%),
    linear-gradient(127deg, ${m} 15%, ${m}00 85%),
    linear-gradient(366deg, ${y} 20%, ${y}00 90%)`;

  return (
    <Avatar {...props} sx={{ width: size, height: size, background, border, boxShadow, ...sx }}>
      <Icon sx={{ color: _color?.main || undefined, fontSize: x * 30 }} />
    </Avatar>
  );
};

export default Logo;
