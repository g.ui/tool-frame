import { LockOpen } from '@mui/icons-material';
import {
  useTheme,
  TextField,
  Typography,
  Grid,
  Box,
  Button,
  Slide,
  useMediaQuery,
} from '@mui/material';
import CSS from 'csstype'
import { useState, useRef, useEffect, Fragment } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { titleCase } from 'title-case';

import { LoadingSpinner as DefaultLoadingSpinner } from '../LoadingScreen';
import { Logo as DefaultLogo } from './Logo';
import { useAutoControlled, useColor, useNotify } from '../utils/hooks';
import { asyncify, pad } from '../utils/misc';
import { LoginProps } from '../typings/Login/LoginProps';
import { InputChangeEvent, KeyEvent } from '../typings/helpers';

const noop = () => {};

export default function Login({
  checking = false,
  logo = DefaultLogo,
  color = 'secondary',
  loadingSpinner = DefaultLoadingSpinner,
  application = '',
  headerText = application ? `Welcome to ${application}!` : 'Welcome!',
  usernameLabel = 'User Name/E-Mail',
  usernameAutocomplete = 'username email',
  passwordLabel = 'Password',
  loginButtonText = 'Login',
  onSubmit = noop,
  ...props
}: LoginProps) {
  const theme = useTheme();
  const navigate = useNavigate();
  const location = useLocation();
  const notifyError = useNotify('error');

  const sm = useMediaQuery(`(max-width:${theme.breakpoints.values.md}px)`);
  const drct = sm ? 'right' : 'down';

  // state management
  const [$checking, set$checking] = useAutoControlled(checking); // indicates 'auth being validated'
  // username + password field states
  const [username, _setUsername] = useState('');
  function setUsername(event: InputChangeEvent) {
    setUsernameError(false);
    _setUsername(event.target.value);
  }
  const [usernameError, setUsernameError] = useState(false);
  const [password, _setPassword] = useState('');
  function setPassword(event: InputChangeEvent) {
    setPasswordError(false);
    _setPassword(event.target.value);
  }
  const [passwordError, setPasswordError] = useState(false);

  // clean props
  const Logo = logo || Fragment;
  const LoadingSpinner = loadingSpinner || Fragment;
  const _color = useColor(color);
  const logoFilter = $checking ? 'blur(10px)' : '';
  const alpha = pad(Math.floor(theme.palette.action.focusOpacity * 255).toString(16), 2);
  const loginBackground = _color ? `${(_color.main)}${alpha}` : undefined;
  const _onSubmit = asyncify(onSubmit);

  // trap focus on username field
  const usernameRef = useRef<HTMLElement>(null);
  useEffect(() => usernameRef.current?.focus(), []); // focus first input once on mount

  // event handlers
  async function onLogin() {
    set$checking(true);
    const usr = username && username.trim();
    const pwd = password && password.trim();
    if (!usr) {
      setUsernameError(true);
      set$checking(false);
    }
    if (!pwd) {
      setPasswordError(true);
      set$checking(false);
    }
    if (!usr || !pwd) return set$checking(false);
    await _onSubmit({ username, password })
      .then(() => {
        if (location.search.startsWith('?r=')) navigate(location.search.substr(3));
        else navigate('/');
        // no need to set$checking(false); because navigate unmounts this component
      })
      .catch(err => {
        notifyError(`Login failed: ${err.message}`);
        set$checking(false);
      });
  }

  function onKeyUp(event: KeyEvent) {
    if (event.key === 'Enter') onLogin();
  }

  const logoSx = {
    p: 2,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    transition: 'width 1, filter 1s',
  };
  const loginSx = { p: 8, display: 'flex', alignItems: 'center', justifyContent: 'start' };
  const slideStyle = {
    position: 'absolute' as CSS.Property.Position,
    paddingLeft: theme.spacing(2),
  };

  return (
    <Grid spacing={0} sx={{ minHeight: '100vh' }} {...props} container onKeyUp={onKeyUp}>
      <Grid item xs={12} md={4} sx={{ ...logoSx, filter: logoFilter }}>
        {logo && <Logo size={200} />}
      </Grid>
      <Grid item xs={12} md={8} sx={{ ...loginSx, background: loginBackground }}>
        <Slide direction={drct} in={$checking} appear={false} style={slideStyle}>
          <Box>
            {loadingSpinner && <LoadingSpinner size={185} />}
          </Box>
        </Slide>
        <Slide direction={drct} in={!$checking} appear={false}>
          <Box>
            <Typography variant="h4">
              {headerText}
            </Typography>
            <Box component="form">
              <div>
                <TextField
                  value={username}
                  onChange={setUsername}
                  label={titleCase(usernameLabel)}
                  autoComplete={usernameAutocomplete}
                  error={usernameError}
                  inputRef={usernameRef}
                  variant="standard"
                  size="small"
                  margin="dense"
                  sx={{ width: '30ch' }}
                  required />
              </div>
              <div>
                <TextField
                  value={password}
                  type="password"
                  onChange={setPassword}
                  label={titleCase(passwordLabel)}
                  autoComplete="current-password"
                  error={passwordError}
                  variant="standard"
                  size="small"
                  margin="dense"
                  sx={{ width: '30ch' }}
                  required />
              </div>
              <Button
                onClick={onLogin}
                variant="contained"
                startIcon={<LockOpen />}
                sx={{ mt: 1 }}>
                {loginButtonText}
              </Button>
            </Box>
          </Box>
        </Slide >
      </Grid>
    </Grid>
  );
}
