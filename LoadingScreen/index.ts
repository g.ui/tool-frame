import LoadingScreen from './LoadingScreen';
export default LoadingScreen;

export { default as LoadingScreen } from './LoadingScreen';
export { default as LoadingSpinner } from './LoadingSpinner';

export * from './LoadingScreen';
export * from './LoadingSpinner';
