import { Backdrop } from '@mui/material';
import { Fragment, FunctionComponent } from 'react';

import { LoadingSpinner } from './LoadingSpinner';
import { LoadingScreenProps } from '../typings/LoadingScreen/LoadingScreenProps';

export const LoadingScreen: FunctionComponent<LoadingScreenProps> = ({
  opened = false,
  spinner = LoadingSpinner,
  grayscale = .9,
  brightness = 1.5,
  blur = 10,
  sx = {},
  ...props
}) => {
  // clean props
  const Spinner = spinner || Fragment;
  const backdropFilter = `grayscale(${grayscale}) brightness(${brightness}) blur(${blur}px)`;

  return (
    <Backdrop
      appear={false}
      {...props}
      open={opened}
      sx={{ zIndex: 'drawer', backdropFilter, ...sx }}>
      {spinner && <Spinner size="large" />}
    </Backdrop>
  );
};

export default LoadingScreen;
