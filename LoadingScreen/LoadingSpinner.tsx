import classnames from 'classnames';
import { FunctionComponent } from 'react';

import prisma from './prisma.gif';
import { LoadingSpinnerProps } from '../typings/LoadingScreen/LoadingSpinnerProps';

export const LoadingSpinner: FunctionComponent<LoadingSpinnerProps> = ({
  size = '',
  rotate = true,
  ...props
}) => {
  const height = {
    small: 30,
    '': 40,
    large: 60,
  }[size];

  return (
    <img
      src={prisma}
      style={{ height: height || size }}
      className={classnames({ 'g-ui-tool-frame-loading-spinner-rotate': rotate })}
      alt="loading spinner"
      {...props} />
  );
};

export default LoadingSpinner;
