# TODO
core concepts
- content: Route wrapped Content
- routing (see react-router-dom) + export all that stuff from react-roueter-dom that is not used/burned internally
- collections props (inkl items) STRG+F Pass an array of [MenuSections.props](#MenuSection.props)
- onItemChange(entity, action, item) erläutern, erwartet, dass via collection back to ToolFrame
- notifcations hooks (see notistack but erweitert)

# @g.ui/tool-frame-ui /// also in imports

React Framework for Web-Tooling.

Utilizing [react-router-dom](https://www.npmjs.com/package/react-router-dom),
[@mui/material](https://www.npmjs.com/package/@mui/material) and
[notistack](https://www.npmjs.com/package/notistack).

Simple use for Framing Application Logic Components with fully customizable:
- [Login](#Login)
- [Navbar](#Navbar) with Breadcrumbs and [QuickAccess](#QuickAccess)
- [Menu](#Menu) with home button, auto-generated [MenuSection](#MenuSection)s from collections,
  custom items and logout button
- Wrapper for your application logic [Content](#Content)s
- Routing
- Notification-Snackbar

# Hooks

# TODO: useBreakpoint, useColor, useNotify, useSnackbar

# Components

## ToolFrame

`import ToolFrame from '@g.ui/tool-frame';` or `import { ToolFrame } from '@g.ui/tool-frame';`

This is the main component to use the ToolFrameUI.
This provides all the functionality for rendering the pretty tool frame around your
application logic components, which you may provide as [Content Routes](#Content-Routes).

### ToolFrame.props

- **initializing**: control whether a [LoadingScreen](#LoadingScreen) is displayed above the tool
  (default: `false`)
- **loading**: control whether loading indicator is shown in the default [Navbar](#Navbar)
  (default: `false`)
- **auth**: controll whether your content routes should be exposed to the user (default: `false`)
  - `true`: all content routes are accessible by the user
  - `false`: content routes are unmounted, a redirect to the `login` is mounted, the login is displayed
- **application**: the application name, displayed in `login`, and `navbar` (default: `''`)
- **theme**: material [theme](https://mui.com/customization/theming/) to be used
  (default: constant [`DEFAULT_THEME`](#DEFAULT_THEME))
- **collections**: controls auto-generated [MenuSections](#MenuSections) in the `menu` and
  [NavbarBreadcrumbs](#NavbarBreadcrumbs) and [QuickAccess](#QuickAccess) in the `navbar`.
  Pass an array of [MenuSections.props](#MenuSection.props) objects. (default: `[]`)
- **logo**: logo component type passed to [Login](#Login), [MenuHomeButton](#MenuHomeButton) and
  [ContentNotFound](#ContentNotFound) (default: [`Logo`](#Logo)),
  use `false` to disable logos, `true` equals default
- **menu**: menu component type (default: [`Menu`](#Menu)),
  use `false` for disabling menu, `true` equals default
- **navbar**: navbar component type (default: [`Navbar`](#Navbar)),
  use `false` for disabling navbar, `true` equals default
- **quickAccess**: component type of quick acccess (default: `'iconsOnly'`)
  - `true`: auto-generated QuickAccess from `collections`
  - `'iconsOnly'`: auto-generated QuickAccess from `collections` with option `iconsOnly`
  - falsy: no QuickAccess shown
  - given custom [QuickAccess](#QuickAccess.props) compatible component
- **login**: login component type (default: [`Login`](#Login)),
  use `false` for no login, `true` equals default
  - _NOTE_: `false` implies, that `auth` is circumenveted and content routes are always mounted
- **loadingScreen**: loading screen component type (default: [`LoadingScreen`](#LoadinScreen)),
  use `false` to disable loading screen, `true` equals default
- **contentNotFound**: component type of 404 page (default: [`ContentNotFound`](#ContentNotFound)),
  use `false` to disable content not found page, `true` equals default
- **menuProps**: props to be passed to the `menu` component
  (default: `{ <logo>, <collection>, <onItemChange>, <onLogout> }`)
- **navbarProps**: props to be passed to the `navbar` component
  (default: `{ <application>, <collections>, <quickAccess>, <loading>, <onLogout> }`)
- _async_ **onItemChange(entity, action, item)**: callback for collection item change
  (add/delete item in menu section) (default: noop)
- _async_ **onLogin( username, password })**: callback called on login form submit,
  check your credentials here! (default: noop → credentials always valid)
- _async_ **onLogout()**: callback called on click to the logout button
  (default: noop → logout always successful)
- **children**: your application logic to be framed [Content Routes](#Content-Routes) (default: `[]`)

## Login Components

### Login

`import { Login } from '@g.ui/tool-frame';`

Fullscreen [Grid](https://mui.com/api/grid/) with [Logo](#Logo) and login fields.

#### Login.props

- **checking**: control whether the login screen displays the loading spinner (default: `false`)
- **logo**: logo component type to be used (default: [`Logo`](#Logo)), use `false` for no logo
- **color**: color of the background (only behind login fields) (default: `'secondary'`)
- **loadingSpinner**: spinner component type to be used (default: [`LoadingSpinner`](#LoadingSpinner)),
  use `false` for no spinner
- **application**: application name to be displayed in the `headerText` (default: `''`)
- **headerText**: welcome text (default: `'Welcome!'` or  `'Welcome to <application>'`)
- **usernameLabel**: label for the username field (default: `'User Name/E-Mail'`)
- **usernameAutocomplete**: [autocomplete](https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#autofill)
  prop for the username field (default: `'username email'`)
- **passwordLabel**: label for the password field (default: `'Password'`)
- **loginButtonText**: text of the login button (default: `'Login'`)
- _async_ **onSubmit({ username, password })**: *callback called on submit, check your credentials here!
  (default: async noop)
- _further props_ are proxied to the underlying [Grid](https://mui.com/api/grid/) component

### Logo

`import { Logo } from '@g.ui/tool-frame';`

[Avatar](https://mui.com/api/avatar/) with a tool-frame typical CMY colored background
containing a [Material Icon](https://mui.com/components/material-icons)]
or [SvgIcon](https://mui.com/api/svg-icon/).

#### Logo.props

- **icon**: icon component type to be used (default: [`AppsOutage`](https://mui.com/components/material-icons/?selected=AppsOutage))
- **size**: size of the logo (default: `40`)
- **color**: color of the icon (default: `#f8f8f8ee` = slightly transparent near-white)
- **sx**: sx passed to the underlying [Avatar](https://mui.com/api/avatar/) (default: `{}`)
- _further props_ are proxied to the underlying [Avatar](https://mui.com/api/avatar/) component

## Navbar Components

### Navbar

`import { Navbar } from '@g.ui/tool-frame';`

[AppBar](https://mui.com/api/app-bar/) Navigation.

Default Navbar items are (in this order):
- [Breadcrumbs](https://mui.com/api/breadcrumbs) to the current path
- [QuickAccess](#QuickAccess) to `<collection>/new`,
  plus [Logout](https://mui.com/components/material-icons/?selected=Logout) button calling `onLogout`

#### Navbar.props

- **collections**: controls auto-generated breadcrumbs and quick access if displayed.
  Pass an array of [MenuSections.props](#MenuSection.props) objects. (default: `[]`)
- **breadcrumbs**: control which breadcrumbs are displayed (default: `true`)
  - `true`: auto-generate breadcrumbs from `collections`, if `quickAccess` is `true` or a custom component
  - falsy: no breadcrumbs (only menu button)
  - array of obejcts (`name*, icon?, link?`) to be shown as breadcrumbs
  - given custom component
- **application**: name of the application, displayed in the first breadcrumb/menu button (default: `''`)
- **quickAccess**: control what [QuickAccess](#QuickAccess) should be shown (default: `'iconsOnly'`)
  - `true`: auto-generated QuickAccess from `collections`
  - `'iconsOnly'`: auto-generated QuickAccess from `collections` with option `iconsOnly`
  - falsy: no QuickAccess shown
  - given custom [QuickAccess](#QuickAccess.props) compatible component
- **loading**: control whether to show [LoadingSpinner](#LoadingSpinner) on the right
  (default: `false`)
- **position**: position passed to the underlying [AppBar](https://mui.com/api/app-bar/)
  (default: `'sticky'`)
- **color**: color passed to the underlying [AppBar](https://mui.com/api/app-bar/)
  (default: `'clear'` → none)
- **sx**: sx passed to the underlying [AppBar](https://mui.com/api/app-bar/)
  (default: `{}` or `{ background: <white>, color: <primary> }` if `color === 'clear'`)
- **onMenuOpen()**: callback called on click on the first breadcrum (= menu button) (default: noop)
- **onLogout()**: callback called on click to the logout button (default: noop)
- _further props_ are proxied to the underlying [AppBar](https://mui.com/api/app-bar/) component

### NavbarBreadcrumbs

`import { NavbarBreadcrumbs } from '@g.ui/tool-frame';`

[Breadcrumbs](https://mui.com/api/breadcrumbs/) for use in [Navbar](#Navbar).

Default Breadcrumbs falls back to generating them from location with help (icon) from `collections`.

#### NavbarBreadcrumbs.props

- **application**: application name to display in the first breadcrumb (= menu button) (default: `''`)
- **collections**: deliver `entity` names and `icon` component types for breadcrumbs auto-generation.
  pass an array of [MenuSections.props](#MenuSection.props) objects. (default: `[]`)
- **breadcrumbs**: if you want to provide your own breadcrumbs, provide them here (default: `[]`)
  - falsy: no breadcrumbs, only auto-generated if `fallbackToLocation`
  - array of obejcts (`name*, icon?, link?`) to be shown as breadcrumbs
  - given custom component
- **fallbackToLocation**: whether to auto-generate breadcrumbs from window location with help of `collections`
  (default: `true`)
  - _NOTE_: this only auto-generates breadcrumbs if `breadcrumbs` is falsy or an empty array
- **truncation**: how many charactes each breadcrumb should not exceed (default: `40`)
- **displayThreshold**: hide certain elements on displays smaller than this threshold (default: `'md'`)
  - hides the application name when smaller than threshold
  - hides the icon of all breadcrumbs when display is smaller than threshold,
    but the menu icon and the icon of the last crumb are always shown
  - valid values are (`xs` → never hide, `sm`, `md`, `lg`, `xl`)
- **onMenuOpen()**: callback called when the firs (=menu) breadcrumb is clicked (default: `noop`)
- _further props_ are proxied to the underlying [Breadcrumbs](https://mui.com/api/breadcrumbs/) component

### NavbarBreadcrumbsItem

`import { NavbarBreadcrumbsItem } from '@g.ui/tool-frame';`

[ListItemButton](https://mui.com/api/list-item-button/) for use in [NavbarBreadcrumbs](#NavbarBreadcrumbs).

Displays an `icon`, a `name` and navigates to a given `link` path.

#### NavbarBreadcrumbsItem.props

- **name**: name to display (default: `''`)
- **icon**: icon component type for displaying the icon. (default: `Fragment` → none)
- **link**: link to be navigated to on click. (default: `false` → none), use `false` for no navigate
- **truncation**: how many charactes each breadcrumb should not exceed (default: `40`)
- **iconDisplayThreshold**: hide icon element on displays smaller than this threshold (default: `'sm'`)
  - valid values are (`xs` → never hide, `sm`, `md`, `lg`, `xl`)
- _further props_ are proxied to the underlying [Breadcrumbs](https://mui.com/api/breadcrumbs/) component
  - _NOTE_ if you overwrite the **onClick()** prop, no navigation will be performed,
    even if a `link` is provided

### QuickAccess

`import { QuickAccess } from '@g.ui/tool-frame';`

Array of [ListItemButton](https://mui.com/api/list-item-button/)s for quick access,
eg. in [Navbar](#Navbar).

Each item consists of (from left):
- [IconButton](https://mui.com/api/icon-button) using the collection `iconSingular``
- `item.entity` as Text

If no link is given in the item it will navigate to `/<entity>/new`.

#### QuickAccess.props

- **items**: controls the items display (default: `[]`), must be compatible with collections array
  (eg. passed by [Navbar](#Navbar)) but can have an extra `link`, so it can have this properties:
  - `entity`: name to be displayed if not `iconsOnly`
  - `icon` or `iconSingular`: icon to be display
  - `link`: link to overwrite default link (default: `'/<entity>/new'`)
- **iconsOnly**: control whether text should be hidden (default: `true`)

## Menu Components

### Menu

`import { Menu } from '@g.ui/tool-frame';`

[SwipeableDrawer](https://mui.com/api/swipeable-drawer/) Menu.

Default menu items are (in this order):
- [Logo](#Logo) button, navigating to `/`
- custom menu items (`topChildren`)
- [MenuSection](#MenuSection)s, auto-generated from [collections prop](#Menu.props)
- custom menu items (`bottomChildren`)
- [Logout](https://mui.com/components/material-icons/?selected=Logout) button, calling `onLogout`

#### Menu.props

- **logo**: component type of the logo displayed on top of the menu items (default: [`Logo`](#Logo))
- **opened**: control whether dialog is opened (default: `false`)
- **width**: drawer width (default: `300`)
- **homeButton**: component type used for home button (default: `MenuHomeButton`),
  use `false` for disabled home button without logo
- **collections**: controls auto-generated MenuSections listed in the menu.
  Pass an array of [MenuSections.props](#MenuSection.props) objects. (default: `[]`)
- **mutexEditing**: whether just one MenuSection should be in editing mode concurrently (default: `true`)
- **mutexExpand**: whether just on MenuSection should be expanded concurrently (default: `true`)
- **topChildren**: custom menu items displayed before the auto-generated MenuSections (default: `[]`)
- **bottomChildren**: custom menu items displayed after the auto-generated MenuSections (default: `[]`)
- **logoutButton**: component type used for logout button (default: `MenuLogoutButton`),
  use `false` for no home button
- **logoutButtonPosition**: list position of the logout button (default: `4`)
  - `0`: before home button
  - `1`: after home button
  - `2`: after `topChildren`
  - `3`: after auto-generated MenuSections
  - `4`: after `bottomChildren`
- _async_ **onItemChange(entity, action, item)**: callback for menu item change (add/delete item in section)
  (default: noop)
- **onOpen()**: callback called whenever menu is opened (default: noop)
- **onClose()**: callback called on escape, backdrop click and item click (default: noop)
- **onLogout()**: callback called on clicking logout button (default: noop)
- _further props_ are proxied to the underlying [SwipeableDrawer](https://mui.com/api/swipeable-drawer/) component

### MenuHomeButton

`import { MenuHomeButton } from '@g.ui/tool-frame';`

[ListItemButton](https://mui.com/api/list-item-button/) for use as [Menu.homeButton](#Menu.props).

#### MenuHomeButton.props

- **logo**: logo copmonent type to be used as only child of the ListItemButton
  (default: `false` → no logo)
- **disabled**: whether button click should be disabled and logo hidden (default: `false`)
- **sx**: sx passed to the underlying innermost [ListItemButton](https://mui.com/api/list-item-button/)
  (default: `{}`)
- _further props_ are proxied to the underlying [ListItemButton](https://mui.com/api/list-item-button/) component

### MenuLogoutButton

`import { MenuLogoutButton } from '@g.ui/tool-frame';`

[ListItemButton](https://mui.com/api/list-item-button/) for use as [Menu.logoutButton](#Menu.props).

#### MenuLogoutButton.props

- **text**: text of the item (default: `'Logout'`)
- **onClick()**: callback called on click (default: noop)
- _further props_ are proxied to the underlying [ListItemButton](https://mui.com/api/list-item-button/) component


### MenuSection

`import { MenuSection } from '@g.ui/tool-frame';`

Section for the [Menu](#Menu) consisting of:
- Header with collections `icon` and `entity` name, plus (from the right)
  - collapse button, if `collapsable`
  - edit penicil, if `editable`
  - add button, if editing and `amendable`
- Items displaying the items `name`, plus
  - delete button if in edit mode

All edit mode buttons (edit pencil, delete bin, add plus) are of the themes warning color
if section is in edit mode.

#### MenuSection.props

- _required_ **entity**: name of the collections entity
- **items**: array of items (default: `[]`), rendered as MenuSectionItem,
  see [MenuSectionItem.props.item](#MenuSectionItem.props) for details
- **icon**: [material icon](https://mui.com/components/material-icons) type for the header (default: `Fragment` → blank icon)
- **editable**: control if an edit pencil is displayed to toggle editing mode (default: `false`)
- **amendable**: control if an add plus is displayed in edit mode to a add an item to the colleciton (default: `true`)
- **editing**: control whether section is currently in edit mode (default: `false`)
- **expandable**: control whether section can be collapsed and expanded (default: `false`)
- **expanded**: control whether section is currently expanded (default: `false`)
- _async_ **onItemChange(entity, action, item)**: callback for item change (add/delete item)
  (default: noop)
- **onEditing(entity)**: callback for edit mode change (default: noop)
- **onCollapse(entity)**: callback when section collapses (default: noop)
- **onExpand(entity)**: callback when section expands (default: noop)

### MenuSectionItem

`import { MenuSectionItem } from '@g.ui/tool-frame';`

[ListItemButton](https://mui.com/api/list-item-button) for use as a [MenuSection](#MenuSection) child.
Contains (from left):
- [ChevronRight](https://mui.com/components/material-icons/?selected=ChevronRight) if `selected`
- `item.name` as text
- [Delete](https://mui.com/components/material-icons/?selected=DeleteOutline) button if `editing`

The item is highlighted as selected if `selected`.

#### MenuSectionItem.props

- _required_ **entity**: name of the collections entity
- _required_ **item**: item to be displayed, `id` (number) and `name` (string) property required
  (fallback: `{ id: -1, name: 'n/a' }`)
- **selected**: control whether the item is highlighted as selected
  (default: `true`/`false` if window location ends with `'/<entity>/<item.id>'`)
- **deletable**: control whether delete bin is displayed (default: `false`)
- **onClick()**: callback called on item click (default: noop)
- **onDelete()**: callback called when delete bin is clicked and confirmed (default: noop)
- _further props_ are proxied to the underlying [ListItemButton](https://mui.com/api/list-item-button) component

## Content Components

### Content

`import { Content } from '@g.ui/tool-frame';`

Wrapper for the content you want to be shown on a specific [Content Route](#Content-Routes).
It wraps the content with [Box](https://mui.com/api/box), [Container](https://mui.com/api/container),
[Paper](https://mui.com/api/papaer) and another [Box](https://mui.com/api/box) in this nesting order.

_NOTE_: We highly recommend you put a component of type `Content` to the `element` prop of each
[Route](#Route) you wan to be handled.
This way you can control the contents to fill the [ToolFrame](#ToolFrame) or be displayed as a Paper
on whatever position within the navbar and menu framing.

#### Content.props

- **initializing**: control whether a [LoadingScreen](#LoadingScreen) is shown above the content
  (default: `false`)
- **maxWidth**: [maxWidth](https://mui.com/api/container/#props) of the Container
  (default: `false` → full size)
- **elevation**: [elevation](https://mui.com/api/paper/#props) ot the Paper
  (default: `4` or `0` if full size)
- **position**: where the Container should be positioned, valid values are
  `'center'`, `'top'`, `'bottom'`, `'left'`, `'right'`,
  `'topleft'`, `'topright'`, `'bottomleft'`, `'bottomright'` (default: `'center'`)
- **align**: horizontal align of the items within the innermost Box, valid values are
  `'left'`, `'center'`, `'right'` (default: `''`)
  - _NOTE_: the Box surrounding your content will have `flex-direction: column`, if you set this
- **sx**: sx passed to the underlying innermost [Box](https://mui.com/api/box) (default: `{}`)
- **children**: your app logic content component(s) (default: `[]`)
- _further props_ are proxied to the underlying innermost [Box](https://mui.com/api/box) component

### ContentNotFound

`import { ContentNotFound } from '@g.ui/tool-frame';`

[Content](#Content) filled with 404 page contents.

#### ContentNotFound.props

- **title**: title of the page (default: `'Page not found!'`)
- **logo**: logo component type to be used (default: [`Logo`](#Logo)), use `false` for no logo
- **backButtonText**: text of the button (default: `'Back to home'`), use `false` for no button
- _further props_ are proxied to the underlying [Content](#Content) component

## Further Components

### LoadingScreen

`import { LoadingScreen } from '@g.ui/tool-frame';`

[Backdrop](https://mui.com/api/backdrop/) with
[backdrop-filter](https://developer.mozilla.org/en-US/docs/Web/CSS/backdrop-filter) and
[LoadingSpinner](#LoadingSpinner) (if `spinner`).

_NOTE_: `backdrop-filter` is known to not be working in Firefox as of early 2022.
So the background will be untouched (not grayscaled, brightened or blurred),
but the backdrop will overlay a fullscreen transparent layer anyway

#### LoadingScreen.props

- **opened**: control whether loading screen is currently displayed (default: `false`)
- **spinner**: spinner component type to be displayed (default: [`LoadingSpinner`](#LoadingSpinner)),
  use `false` for no spinner
- **grayscale**: backdrop-filter [grayscale](https://developer.mozilla.org/en-US/docs/Web/CSS/filter-function/grayscale()) value (default: `.9`)
- **brightness**: backdrop-filter [brightness](https://developer.mozilla.org/en-US/docs/Web/CSS/filter-function/brightness()) value (default: `1.5`)
- **blur**: backdrop-filter [blur](https://developer.mozilla.org/en-US/docs/Web/CSS/filter-function/blur()) value in pixels (default: `10`)
- **sx**: sx passed to the underlying [Backdrop](https://mui.com/api/backdrop/)
  (default: `{ zIndex: 'drawer', backdropFilter: 'grayscale(<grayscale>), brightness(<brightness>), blur(<blur>px)' }`)
- _further props_ are proxied to the underlying [Backdrop](https://mui.com/api/backdrop/) component

### LoadingSpinner

`import { LoadingSpinner } from '@g.ui/tool-frame';`

`<img>` loading spinner with the typical tool-frame [prisma.gif](LoadingScreen/assets/img/prisma.gif).

#### LoadingSpinner.props

- **size**: size of the spinner, valid values are: (default) → `40`, `'small'` → 30, `'large'` → 60 or a custom number
- **rotate**: control whether the spinner `<img>` has an css rotate animation (default: `true`)
  - _NOTE_: this is additional to the rotating triangles in the gif itself
- _further props_ are proxied to the underlying `<img>` component

### ConfirmDialog

`import { ConfirmDialog } from '@g.ui/tool-frame';`

This dialog consists of a simple title and text plus a cancel butten and a confirm button.
Whenever you want users to confirm an action before execution, use this dialog.

#### ConfirmDialog.props

- **opened**: controls whether dialog is opened (default: `false`)
- **title**: the title of the dialog (default: `'Are you sure?'`)
- **text**: content of the dialog (default: `'Do you really want to perform this action?'`)
- **cancelButtonText**: text of the cancel button (default: `'Cancel'`)
- **confirmButtonText**: text of the confirm button (default: `'OK'`)
- **color**: [theme](#ToolFrame.theme) color used for header and buttons (default: `'warning'`)
- **confirmOnEnter**: whether an 'Enter' keyup event will be considered as confirm (default: `false`)
- **onClose()**: callback called on escape, backdrop click, cancel and confirm (default: noop)
- **onConfirm()**: callback called on confirm (default: noop)
- **onKeyUp(event)**: onKeyUp callback proxied to the underlying [Dialog](https://mui.com/api/dialog/)
  (default: noop)
- _further props_ are proxied to the underlying [Dialog](https://mui.com/api/dialog/) component

### InputDialog

`import { ConfirmDialog } from '@g.ui/tool-frame';`

This dialog requests a single text field input.
It behaves similar to the [ConfirmDialog](#ConfirmDialog),
but adds a single required text field to be filled before confirm.

#### InputDialog.props

See [ConfirmDialog.props](#ConfirmDialog.props), plus:

- _required_ **entity**: name of the required text input (shown in auto-generated title and the text field)
- **title**: the title of the dialog (default: `'<entity> information needed'`)
- **text**: content of the dialog (default: `'Please provide below information to proceed'`)
- _async_ **validation(value)**: custom value validation
*  (default: `val => !!val.trim()`  → check for non-empty string)
  - only return value `true` will be considered successful validation
  - string return value will be displayed as snackbar
  - thrown error message will be displayed as snackbar

# Constants

## DEFAULT_THEME

`import { DEFAULT_THEME } from '@g.ui/tool-frame';`

The default material [theme](https://mui.com/customization/theming/) used for the ToolFrameUI,
if none is provided via the `theme` prop of [ToolFrame](#ToolFrame.props)

## ITEM_ACTIONS

`import { ITEM_ACTIONS } from '@g.ui/tool-frame';`

Actions given as the second parameter of the [onItemChange](#onItemChange) callback.
Either of:
- `ITEM_ACTIONS.CREATE`: given on item creation in the menu
- `ITEM_ACTIONS.READ`: just a placeholder, never used
- `ITEM_ACTIONS.UPDATE`: just a placeholder, never used
- `ITEM_ACTIONS.DELETE`: givon on item deletion in the menu
