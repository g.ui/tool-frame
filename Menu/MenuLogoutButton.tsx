import { Logout } from '@mui/icons-material';
import { ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import { FunctionComponent } from 'react';

import { MenuLogoutButtonProps } from '../typings/Menu/MenuLogoutButtonProps';

const noop = () => {};

export const MenuLogoutButton: FunctionComponent<MenuLogoutButtonProps> = ({
  text = 'Logout',
  onClick = noop,
  ...props
}) => {
  return (
    <ListItemButton onClick={onClick} {...props}>
      <ListItemIcon>
        <Logout />
      </ListItemIcon>
      <ListItemText primary={text} />
    </ListItemButton>
  );
};

export default MenuLogoutButton;
