import { DeleteOutline, ChevronRight } from '@mui/icons-material';
import { ListItemButton, ListItemText, IconButton, useTheme, IconButtonProps } from '@mui/material';
import truncate from 'cli-truncate';
import { useState, FunctionComponent } from 'react';
import { useLocation } from 'react-router-dom';
import { titleCase } from 'title-case';

import { ConfirmDialog } from '../dialogs/ConfirmDialog';
import { idFromCollection } from '../utils/misc';
import { ClickEvent } from '../typings/helpers';
import { MenuSectionItemProps } from '../typings/Menu/MenuSectionItemProps';

const noop = () => {};

const defaultItem = { id: -1, name: 'n/a' };

export const MenuSectionItem: FunctionComponent<MenuSectionItemProps> = ({
  entity,
  item = defaultItem,
  selected, // default: location.endsWith(`/${entity}/${item.id}`)
  deletable = false,
  onClick = noop,
  onDelete = noop,
  style = {},
  sx = {},
  ...props
}) => {
  const location = useLocation();
  const theme = useTheme();

  // clean props
  const cid = idFromCollection({ entity });
  const _entity = truncate(titleCase(entity), 30, { position: 'middle' });
  const _name = truncate(item?.name || defaultItem.name, deletable ? 24 : 27);
  const _id = item.id || defaultItem.id;
  const _selected = selected !== undefined
    ? selected
    : location.pathname.endsWith(`/${cid}/${_id}`);

  // sub components
  function ItemDeleteButton(props: IconButtonProps) {
    const [opened, setOpened] = useState(false);

    function onClick(event: ClickEvent) {
      event.stopPropagation();
      return setOpened(true);
    }

    return (
      <>
        <ConfirmDialog
          opened={opened}
          text={`Do you really want to delete the ${_entity} '${_name}'?`}
          onClose={() => setOpened(false)}
          onConfirm={onDelete}
          confirmOnEnter />
        <IconButton color="warning" size="small" {...props} onClick={onClick}>
          <DeleteOutline />
        </IconButton>
      </>
    );
  }

  return (
    <ListItemButton
      {...props}
      onClick={onClick}
      selected={_selected}
      style={{ height: 50, ...style }}
      sx={{ pl: _selected ? 1 : 4, ...sx }}>
      {_selected && <ChevronRight color="disabled" sx={{ width: theme.spacing(3) }} />}
      <ListItemText primary={_name} color="primary" sx={{ whiteSpace: 'nowrap' }} />
      {deletable && (
        <ItemDeleteButton aria-label="Delete" title="Delete" />
      )}
    </ListItemButton>
  );
};

export default MenuSectionItem;
