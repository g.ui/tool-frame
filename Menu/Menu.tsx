import { Home } from '@mui/icons-material';
import { Box, Divider, List, SwipeableDrawer } from '@mui/material';
import { Fragment, useState, FunctionComponent } from 'react';

import { Logo } from '../Login';
import { MenuSection } from './MenuSection';
import { MenuHomeButton } from './MenuHomeButton';
import { MenuLogoutButton } from './MenuLogoutButton';
import { useAutoControlled, useNotify, useStateWithInit } from '../utils/hooks';
import { asyncify, idFromCollection } from '../utils/misc';
import { KeyEvent, ClickEvent } from '../typings/helpers';
import { MenuProps } from '../typings/Menu/MenuProps'
import { CollectionId } from '../typings';

const noop = () => {};
const unique = (value: any, index: number, array: any[]) => array.indexOf(value) === index;

export const Menu: FunctionComponent<MenuProps> = ({
  logo = Logo,
  opened = false,
  width = 300,
  homeButton = MenuHomeButton,
  collections = [],
  mutexEditing = true,
  mutexExpand = true,
  topChildren = [],
  bottomChildren = [],
  logoutButton = MenuLogoutButton,
  logoutButtonPosition = 4,
  onItemChange = noop,
  onOpen = noop,
  onClose = noop,
  onLogout = noop,
  ...props
}) => {
  const notifyError = useNotify('error');

  // clean props
  const HomeButton = homeButton || Fragment;
  const LogoutButton = logoutButton || Fragment;
  const _onLogout = asyncify(onLogout);

  // state management
  const [$opened, set$opened] = useAutoControlled(opened, [
    [onOpen, opened => opened],
    [onClose, opened => !opened],
  ]);

  const [editingSections, setEditingSections] = useState<CollectionId[]>([]);
  function markSectionAsEditing(id: CollectionId, addOrRemove: boolean) {
    let newVal: CollectionId[] = [];
    if (mutexEditing && addOrRemove) newVal.push(id);
    else if (addOrRemove) newVal = newVal.concat(id).filter(unique);
    else newVal = editingSections.filter(cid => cid !== id);
    setEditingSections(newVal);
  }

  const initialExpanded = collections.filter(c => c.expanded).map(idFromCollection);
  const [expandedSections, setExpandedSections]
    = useStateWithInit<CollectionId[]>(initialExpanded);

  function markSectionAsCollapsed(id: CollectionId) {
    markSectionAsEditing(id, false);
    setExpandedSections(expandedSections.filter(cid => cid !== id));
  }
  function markSectionAsExpanded(id: CollectionId) {
    if (mutexExpand) {
      // unmark all currently editing sections
      expandedSections.filter(cid => cid !== id).forEach(id => markSectionAsEditing(id, false));
      setExpandedSections([id]);
    } else setExpandedSections(expandedSections.concat(id).filter(unique));
  }

  // event handlers
  function $onLogout() {
    return _onLogout().catch(err => notifyError(`Logout failed: ${err.message}`));
  }

  function _onClose(event: KeyEvent | ClickEvent) {
    if (event && event.type === 'keydown' && (event as KeyEvent).key !== 'Escape') return;
    set$opened(false);
  }

  // arrange components
  const list = [
    <Fragment key="home">
      <Divider />
      {homeButton
        ? <HomeButton logo={logo || Home} sx={logo ? {} : { pt: 2.5 }}/>
        : <MenuHomeButton disabled />}
    </Fragment>,
    <Fragment key="items-top">{topChildren}</Fragment>,
    collections.map(({ entity, onEditing, onCollapse, onExpand, ...props }) => {
      const id = idFromCollection({ entity });
      function _onEditing(markOrUnmark: false | CollectionId) {
        markSectionAsEditing(id, !!markOrUnmark);
        if (onEditing) onEditing(markOrUnmark);
      }
      function _onCollapse() {
        markSectionAsCollapsed(id);
        if (onCollapse) onCollapse(id);
      }
      function _onExpand() {
        markSectionAsExpanded(id);
        if (onExpand) onExpand(id);
      }

      return (
        <MenuSection
          onItemChange={onItemChange}
          {...props}
          entity={entity}
          key={id}
          editing={editingSections.includes(id)}
          expanded={expandedSections.includes(id)}
          onEditing={_onEditing}
          onCollapse={_onCollapse}
          onExpand={_onExpand} />
      );
    }),
    <Fragment key="items-bottom">{bottomChildren}</Fragment>,
    <Divider key="end" />,
  ];
  const _logoutButton = logoutButton && (
    <Fragment key="logout">
      <Divider />
      <LogoutButton onClick={$onLogout} />
    </Fragment>
  );
  if (_logoutButton) list.splice(logoutButtonPosition, 0, _logoutButton);

  return (
    <SwipeableDrawer
      anchor="left"
      {...props}
      open={$opened}
      onOpen={() => set$opened(true)}
      onClose={_onClose}
      ModalProps={{ keepMounted: true }}>
      <Box onClick={_onClose} onKeyDown={_onClose} sx={{ width }}>
        <List sx={{ pt: 0 }}>{list}</List>
      </Box>
    </SwipeableDrawer>
  );
};

export default Menu;
