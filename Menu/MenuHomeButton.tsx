import { ListItemButton } from '@mui/material';
import { FunctionComponent, Fragment } from 'react';
import { useNavigate } from 'react-router-dom';

import { MenuHomeButtonProps } from '../typings/Menu/MenuHomeButtonProps';

export const MenuHomeButton: FunctionComponent<MenuHomeButtonProps> = ({
  logo,
  disabled = false,
  sx = {},
  ...props
}) => {
  const navigate = useNavigate();

  const Logo = logo || Fragment;

  return (
    <ListItemButton
      onClick={() => navigate('/')}
      {...props}
      disabled={disabled}
      sx={{ height: 63, p: 1, flexDirection: 'column', ...sx }}>
      {!disabled && logo && <Logo size={40} />}
    </ListItemButton>
  );
};

export default MenuHomeButton;
