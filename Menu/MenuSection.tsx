import { Edit, ExpandLess, ExpandMore, EditOff, AddCircleOutline } from '@mui/icons-material';
import {
  Divider,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse,
  ListItemButton,
  List,
  useTheme,
  IconButtonProps,
} from '@mui/material';
import truncate from 'cli-truncate';
import plur from 'plur';
import { Fragment, useState, FunctionComponent, ElementType } from 'react';
import { TransitionGroup } from 'react-transition-group';
import { useLocation, useNavigate } from 'react-router-dom';
import { titleCase } from 'title-case';

import { ITEM_ACTIONS } from '../constants';
import { InputDialog } from '../dialogs/InputDialog';
import { MenuSectionItem } from './MenuSectionItem';
import { useAutoControlled, useNotify } from '../utils/hooks';
import { asyncify, idFromCollection } from '../utils/misc';
import { ClickEvent } from '../typings/helpers';
import { MenuSectionProps } from '../typings/Menu/MenuSectionProps';
import { Item } from '../typings';

const noop = () => {};

export const MenuSection: FunctionComponent<MenuSectionProps> = ({
  entity,
  items = [],
  icon = Fragment,
  editable = false,
  amendable = true,
  editing = false,
  expandable = false,
  expanded = true,
  onItemChange = noop,
  onEditing = noop,
  onCollapse = noop,
  onExpand = noop,
}) => {
  const location = useLocation();
  const navigate = useNavigate();
  const theme = useTheme();
  const notifyError = useNotify('error');

  // state management
  const [$editing, set$editing] = useAutoControlled(editing, [
    editing => onEditing(editing && cid),
  ]);
  const [$expanded, set$expanded] = useAutoControlled(expanded, [
    [() => onExpand(cid), expanded => expanded],
    [() => onCollapse(cid), expanded => !expanded],
  ]);

  // clean props
  const cid = idFromCollection({ entity });
  const prettyEntity = truncate(titleCase(entity), 20, { position: 'middle' });
  const Icon = icon;
  const _onItemChange = asyncify(onItemChange);

  // sub sub components
  function AddButton(props: IconButtonProps) {
    // TODO: fix `state update on an unmounted` on first add of item
    const [opened, setOpened] = useState(false);

    async function addItem(name: Item['name']) {
      const item = { name };
      await _onItemChange(cid, ITEM_ACTIONS.CREATE, item)
        .then(created => { // navigate to newly created item
          if (!created || created.id === undefined) return navigate('/');
          navigate(`/${cid}/${created.id}`);
        })
        .catch(err => notifyError(`Adding ${prettyEntity} '${name}' failed: ${err.message}`));
    }

    function onClick(event: ClickEvent) {
      event.stopPropagation();
      return setOpened(true);
    }

    return (
      <IconButton color="warning" size="small" {...props} onClick={onClick}>
        <InputDialog entity="name" opened={opened} onConfirm={addItem} />
        <AddCircleOutline />
      </IconButton>
    );
  }

  function EditButton() {
    function onClick(event: ClickEvent) {
      event.stopPropagation();
      set$editing(!$editing);
    }

    return (
      <IconButton
        size="small"
        color={editing ? 'warning' : 'default'}
        onClick={onClick}>
        {$editing ? <EditOff /> : <Edit />}
      </IconButton>
    );
  }

  function Expansion() {
    return (
      <IconButton disableRipple>
        {$expanded ? <ExpandLess /> : <ExpandMore />}
      </IconButton>
    );
  }

  // sub components
  function Header() {
    function onClick(event: ClickEvent) {
      event.stopPropagation();
      if (!expandable) return;
      if ($expanded) set$editing(false);
      return set$expanded(!$expanded);
    }

    const Wrapper: ElementType = expandable ? ListItemButton : ListItem;
    let background = theme.palette.mode === 'light'
      ? theme.palette.grey['100']
      : theme.palette.grey['900'];
    if ($editing) {
      const alpha = Math.floor(theme.palette.action.activatedOpacity * 255).toString(16);
      background = `${theme.palette.warning.main}${alpha}`; // mimic active/selected color
    }

    const entities = titleCase(plur(entity));
    const _entities = truncate(entities, !$expanded ? 18 : !$editing ? 14 : 10);
    return (
      <Wrapper onClick={onClick} style={{ height: 50, background }}>
        <ListItemIcon>
          {icon && <Icon />}
        </ListItemIcon>
        <ListItemText primary={_entities} title={entities} />
        {$editing && (!expandable || $expanded) && amendable && <AddButton
          aria-label="Add"
          title="Add" />}
        {editable && (!expandable || $expanded) && <EditButton />}
        {expandable && $expanded && <Divider orientation="vertical" variant="middle"/>}
        {expandable && <Expansion />}
      </Wrapper>
    );
  }

  return (
    <>
      <Divider />
      <Header />
      <Collapse in={!expandable || $expanded} timeout="auto">
        <List disablePadding>
          <TransitionGroup>
            {items.map((item, index) => {
              async function onDelete() {
                await _onItemChange(cid, ITEM_ACTIONS.DELETE, item)
                  .then(() => { // navigate away from deleted item (go to home)
                    if (location.pathname.endsWith(`/${cid}/${item.id}`)) navigate('/');
                  })
                  .catch(err => {
                    notifyError(`Deleting ${prettyEntity} '${item.name}' failed: ${err.message}`);
                  });
              }

              return (
                <Collapse key={index}>
                  <MenuSectionItem
                    entity={entity}
                    item={item}
                    deletable={$editing}
                    selected={location.pathname.endsWith(`/${cid}/${item.id}`)}
                    onClick={() => navigate(`/${cid}/${item.id}`)}
                    onDelete={onDelete} />
                </Collapse>
              );
            })}
          </TransitionGroup>
        </List>
      </Collapse>
    </>
  );
};

export default MenuSection;
