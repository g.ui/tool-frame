import { ListItemButton, IconButton, Typography, Link } from '@mui/material';
import truncate from 'cli-truncate';
import plur from 'plur';
import { Fragment, FunctionComponent } from 'react';
import { useNavigate } from 'react-router-dom';
import { titleCase } from 'title-case';

import { idFromCollection } from '../utils/misc';
import { QuickAccessProps } from '../typings/Navbar/QuickAccessProps';

export const QuickAccess: FunctionComponent<QuickAccessProps> = ({
  items = [],
  iconsOnly = false,
}) => {
  const navigate = useNavigate();

  const breadcrumbSx = { display: { xs: 'none', md: iconsOnly ? 'none' : 'inherit' } };

  return (
    <>
      {items.map(({ entity, link, icon, iconSingular }, i) => {
        const go = () => navigate(link || `/${idFromCollection({ entity })}/new`);
        const Icon = iconSingular || icon || Fragment;
        const _entity = truncate(titleCase(plur(entity)), 30, { position: 'middle' });
        return (
          <ListItemButton key={i} onClick={go}>
            {(iconSingular || icon) && <IconButton color="inherit" disableRipple>
              <Icon />
            </IconButton>}
            <Typography variant="h6" sx={breadcrumbSx}>
              <Link underline="none" color="inherit" sx={{ whiteSpace: 'nowrap' }}>
                {_entity}
              </Link>
            </Typography>
          </ListItemButton>
        );
      })}
    </>
  );
};

export default QuickAccess;
