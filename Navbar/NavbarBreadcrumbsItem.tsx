import { ListItemButton, IconButton, Typography, Link } from '@mui/material';
import truncate from 'cli-truncate';
import { Fragment, FunctionComponent } from 'react';
import { useNavigate } from 'react-router-dom';

import { NavbarBreadcrumbsItemProps } from '../typings/Navbar/NavbarBreadcrumbsItem';

export const NavbarBreadcrumbsItem: FunctionComponent<NavbarBreadcrumbsItemProps> = ({
  name = '',
  icon = Fragment,
  link = '',
  truncation = 40,
  iconDisplayThreshold = 'sm',
  ...props
}) => {
  const navigate = useNavigate();

  // clean props
  const Icon = icon;
  const _sx = { display: { xs: 'none', [iconDisplayThreshold]: undefined } };

  return (
    <ListItemButton onClick={() => link && navigate(link)} {...props}>
      {icon && <IconButton color="inherit" edge="start" disableRipple sx={_sx}>
        <Icon />
      </IconButton>}
      {name && <Typography variant="h6">
        <Link underline="none" color="inherit" sx={{ whiteSpace: 'nowrap' }}>
          {truncate(name, truncation || 200, {
            position: 'middle',
            preferTruncationOnSpace: true,
          })}
        </Link>
      </Typography>}
    </ListItemButton>
  );
};

export default NavbarBreadcrumbsItem;
