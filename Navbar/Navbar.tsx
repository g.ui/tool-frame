import { Logout } from '@mui/icons-material';
import {
  AppBar,
  Box,
  IconButton,
  Toolbar,
  Zoom,
  useTheme,
  useMediaQuery,
} from '@mui/material';
import { FunctionComponent, Fragment } from 'react';

import { LoadingSpinner } from '../LoadingScreen';
import { NavbarBreadcrumbs } from './NavbarBreadcrumbs';
import { QuickAccess } from './QuickAccess';
import { useNotify } from '../utils/hooks';
import { asyncify } from '../utils/misc';
import { NavbarProps } from '../typings/Navbar/NavbarProps';

const noop = () => {};

export const Navbar: FunctionComponent<NavbarProps> = ({
  collections = [],
  application = '',
  breadcrumbs = true,
  quickAccess = 'iconsOnly',
  loading = false,
  position = 'sticky',
  color = 'clear',
  sx = {},
  onMenuOpen = noop,
  onLogout = noop,
  ...props
}) => {
  const theme = useTheme();
  const notifyError = useNotify('error');

  const xxs = useMediaQuery('(max-width:460px)');
  const xs = useMediaQuery(`(max-width:${theme.breakpoints.values.sm}px)`);
  const sm = useMediaQuery(`(max-width:${theme.breakpoints.values.md}px)`);

  // clean props
  let _sx = { ...sx };
  if (color === 'clear') {
    _sx = { background: theme.palette.common.white, color: theme.palette.primary.main, ...sx };
  }
  const _color = color === 'clear' ? 'inherit' : color;
  const CustomQuickAccess = (quickAccess && QuickAccess) || Fragment;
  const _onLogout = asyncify(onLogout);

  // no breadcrumbs if quickAccess is big (= not iconsOnly)
  const noBreadcrumbs = quickAccess && quickAccess !== 'iconsOnly';
  const autoBreadcrumbs = breadcrumbs === true;

  // event handlers
  function $onLogout() {
    return _onLogout()
      .catch(err => notifyError(`Logout failed: ${err.message}`));
  }

  const pt = quickAccess ? loading ? 1.6 : 1 : 0;
  return (
    <AppBar {...props} position={position} color={_color} sx={_sx}>
      <Toolbar>
        <NavbarBreadcrumbs
          application={application}
          collections={collections}
          breadcrumbs={autoBreadcrumbs ? undefined : breadcrumbs}
          fallbackToLocation={autoBreadcrumbs && !noBreadcrumbs}
          truncation={xxs ? 13 : xs ? 20 : sm ? 30 : 40}
          displayThreshold='md'
          onMenuOpen={onMenuOpen} />
        <Box sx={{ display: 'flex' }}>
          {!xxs && quickAccess && <CustomQuickAccess
            items={collections}
            iconsOnly={quickAccess === 'iconsOnly'} />}
          <Box sx={{ pt, pl: 2, transition: 'padding-top .2s' }}>
            <Zoom
              key="logout"
              in={quickAccess && !xxs && !loading}
              appear={false}
              style={{ position: 'absolute' }}>
              <IconButton onClick={$onLogout}><Logout /></IconButton>
            </Zoom>
            <Zoom in={loading}>
              <Box><LoadingSpinner size="small" /></Box>
            </Zoom>
          </Box>
        </Box>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
