import { NavigateNext, Menu } from '@mui/icons-material';
import { Breadcrumbs } from '@mui/material';
import { Fragment, ReactElement, FunctionComponent } from 'react';
import { useLocation } from 'react-router-dom';

import { NavbarBreadcrumbsItem } from './NavbarBreadcrumbsItem';
import { useBreakpoint } from '../utils/hooks';
import { idFromCollection } from '../utils/misc';
import { NavbarBreadcrumbsProps } from '../typings/Navbar/NavbarBreadcrumbsProps';
import { ClickEvent } from '../typings/helpers';

const noop = () => {};

export const NavbarBreadcrumbs: FunctionComponent<NavbarBreadcrumbsProps> = ({
  application = '',
  collections = [],
  breadcrumbs = [],
  fallbackToLocation = true,
  truncation = 40,
  displayThreshold = 'md', // hide certain elements on displays smaller than threshold
  onMenuOpen = noop,
  ...props
}) => {
  const location = useLocation();
  const breakpoint = useBreakpoint(displayThreshold);

  // clean props
  const _breadcrumbs = breadcrumbs ? breadcrumbs : [];

  // autogenerate breadcrumbs from location
  if (Array.isArray(_breadcrumbs) && !_breadcrumbs.length && fallbackToLocation) {
    const path = location.pathname.split('/').filter(x => x);
    let link = '';
    for (let i = 0; i < path.length; i += 2) {
      const entity = path[i];
      const id = path[i + 1];
      if (!entity || !id) break;
      const collection = collections
        .find(c => idFromCollection(c) === idFromCollection({ entity }));
      /* allow numeric ids to match string from location */ // eslint-disable-next-line eqeqeq
      const item = collection?.items?.find(it => it.id == id);
      link = `${link}/${entity}/${item?.id || id}`;
      _breadcrumbs.push({
        icon: collection?.iconSingular || collection?.icon || undefined,
        name: item?.name,
        link: `${link}`,
      });
    }
  }

  // fill list from breadcrumbs (with properties: name (required), icon (optional), link (optional))
  let list: ReactElement[] = [];
  if (!Array.isArray(_breadcrumbs)) list.push(<Fragment key="custom">{_breadcrumbs}</Fragment>);
  else {
    list = _breadcrumbs.map((crumb, i) => {
      const last = i === _breadcrumbs.length - 1;
      return <NavbarBreadcrumbsItem
        key={i}
        {...crumb}
        truncation={truncation}
        iconDisplayThreshold={last ? 'xs' : displayThreshold} />;
    });
  }

  function onClick(event: ClickEvent) {
    event.preventDefault(); // prevent hash change
    onMenuOpen();
  }

  // add menu button (with given application name) as first element
  list.unshift(
    <NavbarBreadcrumbsItem
      key="menu"
      name={(breakpoint && application) || undefined}
      icon={Menu}
      onClick={onClick}
      iconDisplayThreshold='xs'
      sx={{ width: application ? 'auto' : 55 }} />,
  );

  return (
    <Breadcrumbs
      color="inherit"
      separator={breakpoint && <NavigateNext fontSize="small" />}
      sx={{ flexGrow: 1 }}
      {...props}>
      {list}
    </Breadcrumbs>
  );
};

export default NavbarBreadcrumbs;
