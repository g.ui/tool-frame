import { nanoid } from 'nanoid';
import { paramCase } from 'param-case';

import { Collection, CollectionId } from '../typings';

export function pad(str: string, size: number) {
  while (str.length < (size || 2)) {
    str = '0' + str;
  }
  return str;
}

export function asyncify(fn: Function): (...args: any[]) => Promise<any> {
  return function(...args) {
    return Promise.resolve().then(() => fn(...args));
  };
}

const collectionIds: { [key: CollectionId]: Collection } = {};
export function idFromCollection(collection: Collection): CollectionId {
  let id: CollectionId | undefined = Object.keys(collectionIds)
    .find(id => collectionIds[id] === collection);
  if (!id && collection.id) id = collection.id;
  if (!id && collection.entity) id = paramCase(collection.entity);
  if (!id) id = nanoid();
  collection.id = id;
  collectionIds[id] = collection;
  return id;
}

const misc = {
  asyncify,
  pad,
  idFromCollection,
};
export default misc;
