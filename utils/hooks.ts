import { makeStyles } from '@mui/styles';
import { Breakpoint, useMediaQuery, useTheme } from '@mui/material';
import * as notistack from 'notistack';
import { Dispatch, SetStateAction, useEffect, useState } from 'react';

import { DEFAULT_THEME } from '../constants';
import { AmendedThemeColor } from '../typings/helpers';
import {
  AutoControllCallback,
  AutoControllCallbacks,
  AutoControllCondition,
  useNotifiyType,
} from '../typings/utils/hooks';

// use state reacting to prop changes and calling conditional callback on setState
export function useAutoControlled(prop: any, callbacks?: AutoControllCallbacks) {
  const _callbacks: [AutoControllCallback, AutoControllCondition][] = !callbacks
    ? []
    : callbacks.map(cb => Array.isArray(cb) ? cb : [cb, () => true]);
  const [state, setState] = useState(prop);
  useEffect(() => setState(prop), [prop]);
  function setStateAndCallback(value: any) {
    setState(value);
    _callbacks.forEach(([cb, cond]) => cond(value) && cb(value));
  }
  return [state, setStateAndCallback];
}

export function useBreakpoint(point: Breakpoint) {
  const theme = useTheme();
  const xs = useMediaQuery(`(min-width:${theme.breakpoints.values.xs}px)`);
  const sm = useMediaQuery(`(min-width:${theme.breakpoints.values.sm}px)`);
  const md = useMediaQuery(`(min-width:${theme.breakpoints.values.md}px)`);
  const lg = useMediaQuery(`(min-width:${theme.breakpoints.values.lg}px)`);
  const xl = useMediaQuery(`(min-width:${theme.breakpoints.values.xl}px)`);
  return { xs, sm, md, lg, xl }[point];
}

// wrap color functionality of useTheme (for using custom colors)
export function useColor(color: AmendedThemeColor | string) {
  const { palette } = useTheme();
  if (color === 'clear') return undefined;
  if (palette[color as AmendedThemeColor]) {
    return palette.augmentColor({ color: palette[color as AmendedThemeColor] });
  }
  return palette.augmentColor({ color: { main: color } });
}

// wrap useSnackbar of the notistack lib with convenience
export function useSnackbar() {
  const res = notistack.useSnackbar();
  return { ...res, notify: res.enqueueSnackbar };
}
export function useNotify(variant: notistack.VariantType = 'default'): useNotifiyType {
  const { notify } = useSnackbar();
  return function(message, options) {
    return notify(message, { variant, ...options });
  };
}

// special classes for the notisnack snackbar
const notVariants = ['Success', 'Error', 'Warning', 'Info']
  .map(v => `:not(.SnackbarItem-variant${v})`)
  .join('');
export const useSnackbarStyles = makeStyles({
  root: ({ theme }: any) => {
    const dark = theme.palette.type === 'dark';
    return {
      [`& .SnackbarContent-root.SnackbarItem-contentRoot${notVariants}`]: {
        backgroundColor: dark ? theme.palette.grey.A700 : theme.palette.common.white,
        color: dark ? theme.palette.common.white : theme.palette.primary.main,
      },
    };
  },
}, { defaultTheme: DEFAULT_THEME });

// wrap useState with functionality: default value is just set once on mount, not on every re-render
export function useStateWithInit<S>(initialState: S): [S, Dispatch<SetStateAction<S>>] {
  const [state, setState] = useState<S>(initialState);

  // initialize state once on mount
  useEffect(() => setState(initialState), []); // eslint-disable-line react-hooks/exhaustive-deps

  // return handler
  return [state, setState];
}

const hooks = {
  useAutoControlled,
  useBreakpoint,
  useColor,
  useNotify,
  useSnackbar,
  useSnackbarStyles,
  useStateWithInit,
};
export default hooks;
